import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Page-Login/Btn_Create a Profile'), 5)

Mobile.setText(findTestObject('Object Repository/Page-Register/android.widget.EditText - First Name'), first_name, 5)

Mobile.setText(findTestObject('Object Repository/Page-Register/android.widget.EditText - Last Name'), last_name, 5)

Mobile.setText(findTestObject('Object Repository/Page-Register/android.widget.EditText - Country'), country, 5)

Mobile.setText(findTestObject('Object Repository/Page-Register/android.widget.EditText - Username'), user_name, 5)

Mobile.setText(findTestObject('Object Repository/Page-Register/android.widget.EditText - Password'), password, 5)

Mobile.setText(findTestObject('Page-Register/android.widget.EditText - Confirm Password'), confirm_password, 5)

Mobile.tap(findTestObject('Page-Register/android.widget.Button - Create Profile'), 5)

