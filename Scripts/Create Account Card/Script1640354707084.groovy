import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Page-Dashboard/btn_Sidebar'), 5)

Mobile.tap(findTestObject('Page-Dashboard/android.widget.CheckedTextView - Make a Deposit'), 5)

isElementPresent = Mobile.waitForElementPresent(findTestObject('Page-Dashboard/txt_Not Have Enough Account'), 5)

Mobile.verifyElementExist(findTestObject('Page-Dashboard/txt_Not Have Enough Account'), 5)

if (isElementPresent.equals(true)) {
    Mobile.tap(findTestObject('Page-Dashboard/android.widget.Button - ADD ACCOUNT IN NOTIF'), 5)

    Mobile.setText(findTestObject('Page-Account/android.widget.EditText - Account Name'), card_name, 5)

    Mobile.setText(findTestObject('Page-Account/android.widget.EditText - Initial Balance (Optional)'), amount, 5)

    Mobile.tap(findTestObject('Page-Account/android.widget.Button - ADD'), 5)
} else {
    Mobile.tap(findTestObject('Page-Dashboard/btn_Sidebar'), 5)

    Mobile.tap(findTestObject('Page-Dashboard/android.widget.CheckedTextView - Logout'), 5)
}

