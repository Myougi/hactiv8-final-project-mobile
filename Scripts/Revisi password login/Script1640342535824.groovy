import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.comment('Login dgn password Salah')

WebUI.callTestCase(findTestCase('Block/Login'), [('username') : username, ('password') : '#Finansia2021'], FailureHandling.STOP_ON_FAILURE)

isElementPresent = Mobile.waitForElementNotPresent(findTestObject('Page-Dashboard/android.widget.TextView - Dashboard'), 
    5)

Mobile.comment('Jika gagal login, maka coba login dengan password yang sesuai atau logout')

if (isElementPresent.equals(true)) {
    WebUI.callTestCase(findTestCase('Block/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
} else {
    Mobile.tap(findTestObject('Page-Dashboard/btn_Sidebar'), 5)

    Mobile.tap(findTestObject('Page-Dashboard/android.widget.CheckedTextView - Logout'), 5)
}

